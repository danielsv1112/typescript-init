import Operacion from "./Operacion";

class Resta extends Operacion {
    constructor(n1: number, n2: number) {
        super(n1, n2);
    }

    calcular(): number {
        const valores = super.calcular();
        const [op1, op2] = (typeof valores === 'object')? valores : [0,0];
        return op1 - op2;
    }
}

export default Resta;
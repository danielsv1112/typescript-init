import Suma from './Suma';
import Resta from './Resta';
import Multiplicacion from './Multiplicacion';
import Division from './Division';
import process from 'process';

// const miSuma = new Suma(55,45);
// console.log(miSuma.calcular());

// const miResta = new Resta(66, 34)
// console.log(miResta.calcular());

// const miProducto = new Multiplicacion(6,9);
// console.log(miProducto.calcular());

// const miDivision = new Division(1,2);
// console.log(miDivision.calcular())

type operadores = '+' | '-' | '*' | '/';

const obtenerOperandos = (): {operandos: string[], operadorSeleccionado: operadores} => {
    const operacionesValidas: operadores[] = ['+','-','*','/'];
    const parametros = process.argv.filter(parametro => parametro.includes('OP='))[0];
    const operacion = parametros.split('=')[1];
    let operandos: string[] = [];
    let operadorSeleccionado: operadores = '+';
    operacionesValidas.forEach(simbolo => {
        if(operacion.includes(simbolo)){
            operandos = operacion.split(simbolo);
            operadorSeleccionado = simbolo;
        }
    });
    return { operandos, operadorSeleccionado };
}

const operar = (): number => {
    const { operandos, operadorSeleccionado } = obtenerOperandos();
    let resultado: number;
    switch(operadorSeleccionado){
        case '+':
            resultado = new Suma(parseFloat(operandos[0]), parseFloat(operandos[1])).calcular();
            break;
        case '-':
            resultado = new Resta(parseFloat(operandos[0]), parseFloat(operandos[1])).calcular();
            break;
        case '*':
            resultado = new Multiplicacion(parseFloat(operandos[0]), parseFloat(operandos[1])).calcular();
            break;
        case '/':
            resultado = new Division(parseFloat(operandos[0]), parseFloat(operandos[1])).calcular();
            break;
    }
    return resultado;
}

console.log(operar());
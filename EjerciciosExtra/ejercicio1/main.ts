import Account from "./account"

const main = (): void => {
    const miCuenta = new Account('Barney Guzman');
    miCuenta.deposit(10000);
    miCuenta.whitDrawls(3000);
    miCuenta.whitDrawls(2000);
    miCuenta.deposit(4500);
    miCuenta.whitDrawls(15.5);

    console.log(miCuenta.toString());
}

main();
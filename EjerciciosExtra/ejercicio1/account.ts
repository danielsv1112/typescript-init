class Account {
    private headline: string;
    private amount: number;

    constructor(headline: string) {
        this.headline = headline;
        this.amount = 0;
    }

    public deposit(amount: number): void{
        if(amount > 0){
            this.amount += amount;
        }
    }

    public whitDrawls(amount: number): void{
        if(amount > this.amount){
            this.amount = 0;
        }else{
            this.amount -= amount;
        }
    }

    getHeadline(): string{
        return this.headline;
    }
    setHeadline(headline: string): void{
        this.headline = headline;
    }
    getAmount(): number{
        return this.amount;
    }
    setAmount(amount: number): void{
        this.amount = amount;
    }
    toString(): string{
        return(`
        Titular: ${this.headline}
        Saldo: ${this.amount}
        `);
    }
}

export default Account;
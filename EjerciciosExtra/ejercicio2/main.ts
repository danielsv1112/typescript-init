import Persona from './persona';

const main = () => {
    const barney = new Persona('Barney','Guzman',22,'hombre',78,1.73);
    console.log(barney.toString());
    console.log(`\tEsta persona${(!barney.isOlder()) ?'no':''} es mayor de edad`);

    const daniel = new Persona('Daniel','Garcia',21,'hombre');
    console.log(daniel.toString());
    console.log(`\tEsta persona${(!daniel.isOlder()) ?'no':''} es mayor de edad`);

    const generico = new Persona();
    generico.setName('Angela');
    generico.setLastName('Rojas');
    generico.setAge(16);
    generico.setGender('mujer');
    generico.setWeight(50);
    generico.setHeight(1.63);
    console.log(generico.toString());
    console.log(`\tEsta persona ${(!generico.isOlder()) ?'no ':''}es mayor de edad`);
}

main();
type Sexo = 'hombre' | 'mujer';

class Persona {
    private name: string;
    private lastName: string;
    private age: number;
    private curp?: string;
    private gender: Sexo;
    private weight: number;
    private height: number;

    constructor(name: string = '', lastName: string = '', age: number = 0, gender: Sexo = 'hombre', weight: number = 0, height: number = 0) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.gender = this.checkGender(gender);
        this.weight = weight;
        this.height = height;
        this.curp = this.createCURP();
    }

    setName(name: string){
        this.name = name;
    }
    getName(): string{
        return this.name;
    }
    setLastName(lastName: string){
        this.lastName = lastName;
    }
    getLastName(): string{
        return this.lastName;
    }
    setAge(age: number){
        this.age = age;
    }
    getAge(): number{
        return this.age;
    }
    setGender(gender: Sexo){
        this.gender = gender;
    }
    getGender(): Sexo{
        return this.gender;
    }
    setWeight(weight: number){
        this.weight = weight;
    }
    getWeight(): number{
        return this.weight;
    }
    setHeight(height: number){
        this.height = height;
    }
    getHeight(): number{
        return this.height;
    }

    calculateBMI(): number{
        const indice = (this.weight)/(this.height**2);
        return (indice < 20) ? -1 : (indice >= 20 && indice <= 25) ? 0 : (indice > 25) ? 1 : NaN;
    }

    isOlder(): boolean{
        return this.age >= 18;
    }

    checkGender(gender: Sexo): Sexo{
        return (gender === 'hombre' || gender === 'mujer')? gender : 'hombre';
    }

    private createCURP(){
        let randNum:string = '';
        for (let i = 1; i <= 7; i++) {
            let rand: number = Math.random() * 10;
            let randString: string = rand.toFixed();
            randNum += randString;
        }
        return randNum;
    }

    toString(): string{
        let imc = this.calculateBMI();
        return (`
        Nombre: ${this.name}
        Apellido: ${this.lastName}
        Edad: ${this.age}
        CURP: ${this.curp}
        Sexo: ${this.gender}
        Peso: ${this.weight} kg
        Altura: ${this.height} mts
        ${(imc === -1)? 'Esta persona esta por debajo de su peso ideal': (imc === 0)? 'Esta persona esta en su peso ideal': (imc === 1)? 'Esta persona tiene sobrepeso': 'IMC no disponible'}
        `);
    }
}

export default Persona;
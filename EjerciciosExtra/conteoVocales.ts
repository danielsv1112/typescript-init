const miCadena: string = 'Hola soy el tio Barney';

const contarVocales = (cadena: string): {a: number, e: number, i: number, o: number, u: number} => {
    const vocales = ['a','e','i','o','u'];
    let conteo = {a:0, e:0, i:0, o:0, u:0} as {a:number, e:number, i:number, o:number, u:number};
    let cadenaLowerCase = cadena.toLocaleLowerCase();
    let stringToArray = cadenaLowerCase.split("");

    vocales.forEach((vocal) => {
        conteo = {
            ...conteo,
            [vocal]: stringToArray.filter(letra => letra === vocal).length
        }
    });
    return conteo;
}

console.log(contarVocales(miCadena));
const nombres: string[] = ['Barney', 'Akio', 'Gamesa'];

const garage: Array<Array<string>> = [
    ['Barney', 'Akio', 'Gamesa'],
    ['Aventador', 'Challenger', '911']
]

const refrescos: (string|number|boolean)[] = ['Pepsi',20,true];
class Vehicle{
    private color?: string;
    private type?: string;
    private brand?: string;
    private serialNumber: number|string;

    constructor(serialNumber:number|string){
        this.serialNumber = serialNumber;
    }

    setColor( color: string){
        this.color = color;
    }
    
    getColor(): string|undefined{
        return this.color;
    }
}

const volvo = new Vehicle('852963741');

console.log(volvo); 
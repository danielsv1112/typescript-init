interface Person {
    id: number,
    name: string,
    username: string,
    email?: string
}

type Colors = {
    red: '#7363837',
    black: '#000000'
}

enum Shapes {
    circle = 'Circulo',
    triangle = 'Triaungulo'
}

const user: Person = {
    id: 1,
    name: 'Barney',
    username: 'barney12',
    email: 'barney@guzman.com'
}

console.log(user);
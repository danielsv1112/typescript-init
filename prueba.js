const miString = 'Hola soy el tio Barney';
const vocales = ['a','e','i','o','u'];

const contarVocal = (cadena)=> {
    let conteo = {};
    let cadenaLowerCase = cadena.toLocaleLowerCase();
    let stringToArray = cadenaLowerCase.split("");

    vocales.forEach(vocal => {
        conteo[vocal] = stringToArray.filter(letra => letra === vocal).length;
    });
    return conteo;
}

console.log(contarVocal(miString));
import axios from 'axios';

const baseURL: string = 'https://jsonplaceholder.typicode.com/users/';

const miConsulta = (id: number): void => {
    axios.get(`${baseURL}${id}`).then((response) => {
        imprimirMsj(response.data);
    });
}

const imprimirMsj = (usr: {
    id: number,
    name: string,
    username: string,
    email: string,
    address: {
        street: string,
        suite: string,
        city: string,
        zipcode: string,
        geo: {
            lat: string,
            lng: string
        }
    },
    phone: string,
    website: string,
    company: {
        name: string,
        catchPhrase: string,
        bs: string
    }
}): void => {
    var cadena: string = `
        La persona ${usr.name} con el nombre de usuario ${usr.username} y correo electrónico ${usr.email}
        vive en: ${usr.address.street} ${usr.address.suite} ${usr.address.zipcode}
        con las coordenadas ${usr.address.geo.lat}, ${usr.address.geo.lng}.`;
    var extIndex: number = usr.phone.indexOf('x');
    if(extIndex === -1){
        cadena += `
        Puede contactarlo al teléfono: ${usr.phone}`;
    }else{
        cadena += `
        Puede contactarlo al teléfono: ${usr.phone.substring(0,extIndex - 1)} con extensión ${usr.phone.substring(extIndex + 1)}`;
    }

    console.log(cadena);
}

miConsulta(3);
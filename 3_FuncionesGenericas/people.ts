interface person {
    'id': number,
    'first_name': string,
    'last_name': string,
    'email': string,
    'gender': string,
    'ip_address': string
}

export const people: person[] = [
    {
        "id": 1,
        "first_name": "Reilly",
        "last_name": "Abramin",
        "email": "rabramin0@quantcast.com",
        "gender": "Male",
        "ip_address": "242.85.75.138"
    },
    {
        "id": 2,
        "first_name": "Dorian",
        "last_name": "Bickmore",
        "email": "dbickmore1@bbb.org",
        "gender": "Male",
        "ip_address": "189.205.97.241"
    },
    {
        "id": 3,
        "first_name": "Randy",
        "last_name": "Josilowski",
        "email": "rjosilowski2@state.gov",
        "gender": "Female",
        "ip_address": "35.59.98.5"
    },
    {
        "id": 4,
        "first_name": "Perkin",
        "last_name": "Gercke",
        "email": "pgercke3@weebly.com",
        "gender": "Male",
        "ip_address": "122.249.219.243"
    },
    {
        "id": 5,
        "first_name": "Albert",
        "last_name": "Tavener",
        "email": "atavener4@wiley.com",
        "gender": "Male",
        "ip_address": "64.12.209.171"
    },
    {
        "id": 6,
        "first_name": "Deane",
        "last_name": "Hauck",
        "email": "dhauck5@theguardian.com",
        "gender": "Male",
        "ip_address": "180.9.58.120"
    },
    {
        "id": 7,
        "first_name": "Austin",
        "last_name": "Strass",
        "email": "astrass6@imgur.com",
        "gender": "Male",
        "ip_address": "28.199.241.192"
    },
    {
        "id": 8,
        "first_name": "Emmit",
        "last_name": "Gherardi",
        "email": "egherardi7@patch.com",
        "gender": "Male",
        "ip_address": "135.17.88.93"
    },
    {
        "id": 9,
        "first_name": "Stefa",
        "last_name": "O'Mohun",
        "email": "somohun8@discovery.com",
        "gender": "Female",
        "ip_address": "240.60.149.124"
    },
    {
        "id": 10,
        "first_name": "Timothee",
        "last_name": "Victoria",
        "email": "tvictoria9@latimes.com",
        "gender": "Male",
        "ip_address": "31.252.234.45"
    },
    {
        "id": 11,
        "first_name": "Bailey",
        "last_name": "Ollington",
        "email": "bollingtona@github.com",
        "gender": "Male",
        "ip_address": "188.68.111.30"
    },
    {
        "id": 12,
        "first_name": "Cristy",
        "last_name": "Devonside",
        "email": "cdevonsideb@alexa.com",
        "gender": "Female",
        "ip_address": "133.204.13.69"
    },
    {
        "id": 13,
        "first_name": "Renaldo",
        "last_name": "Canacott",
        "email": "rcanacottc@huffingtonpost.com",
        "gender": "Male",
        "ip_address": "68.239.184.9"
    },
    {
        "id": 14,
        "first_name": "Mehetabel",
        "last_name": "Waples",
        "email": "mwaplesd@noaa.gov",
        "gender": "Female",
        "ip_address": "136.197.121.221"
    },
    {
        "id": 15,
        "first_name": "Gardener",
        "last_name": "Pratley",
        "email": "gpratleye@macromedia.com",
        "gender": "Male",
        "ip_address": "116.147.170.102"
    },
    {
        "id": 16,
        "first_name": "Elwood",
        "last_name": "Boffin",
        "email": "eboffinf@cisco.com",
        "gender": "Male",
        "ip_address": "74.72.38.93"
    },
    {
        "id": 17,
        "first_name": "Antons",
        "last_name": "Telega",
        "email": "atelegag@sciencedaily.com",
        "gender": "Male",
        "ip_address": "213.111.45.137"
    },
    {
        "id": 18,
        "first_name": "Velma",
        "last_name": "Trimnell",
        "email": "vtrimnellh@ameblo.jp",
        "gender": "Female",
        "ip_address": "220.162.42.84"
    },
    {
        "id": 19,
        "first_name": "Juliana",
        "last_name": "Stannas",
        "email": "jstannasi@dailymail.co.uk",
        "gender": "Female",
        "ip_address": "42.140.169.88"
    },
    {
        "id": 20,
        "first_name": "Dorise",
        "last_name": "Talton",
        "email": "dtaltonj@hatena.ne.jp",
        "gender": "Female",
        "ip_address": "125.19.194.227"
    }
]
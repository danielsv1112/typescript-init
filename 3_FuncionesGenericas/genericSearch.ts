import { people } from './people';

const genericSearch = <T>(obj: T, props: Array<keyof(T)>, query: string): boolean => {
    if(query === ''){
        return false;
    }else{
        return props.some(prop => {
            const value = obj[prop];
            if( typeof(value) === 'string' || typeof(value) === 'number' ){
                return value.toString().toLowerCase().includes(query.toLowerCase());
            }else{
                return false;
            }
        });
    }
}

export default genericSearch;
const search = ( query: string ) => {
    return people.filter(person => genericSearch( person, ['email', 'first_name'] , query));
}

console.log(search('ab'));

